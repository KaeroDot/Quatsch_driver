'''
    This is to simulate FPGA so a driver can be tested.
    Port is set to 8888.

    Example:
        run the simulator:
        python3 QuADC_firmware_simulator.py
        and run script reading data from the simulator:
        python3 QuADC.py -a 127.0.0.1 -p 8888 -s 10000
        or try LabVIEW driver

    Script was developed during EMPIR project QuADC.
'''

import socket
import sys
import time
from struct import pack, unpack

HOST = ''   # Symbolic name, meaning all available interfaces
PORT = 8888  # Arbitrary non-privileged port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('Socket created')

try:
    # Bind socket to local host and port
    try:
        s.bind((HOST, PORT))
    except socket.error as msg:
        print('Bind failed. Error: ' + str(msg[0]) + ' Message ' + msg[1])
        sys.exit()

    print('Socket bind complete')

    # Start listening on socket
    s.listen(10)
    print('Socket now listening')

    # now keep talking with the client
    while 1:
        # wait to accept a connection - blocking call
        conn, addr = s.accept()
        print('Connected with ' + addr[0] + ':' + str(addr[1]))
        # Receiving from client
        while True:
            data = conn.recv(1024)
            if not data:
                break
            print("Recieved " + str(len(data)) + " bytes:")
            print(data)
            idx = 0
            samples = unpack("i", data[idx:idx+4])[0]
            print("Requested samples: " + str(samples))
            if len(data) > 4:
                idx = 4
                test = unpack("i", data[idx:idx+4])[0]
                print("Test: " + str(test))
            if len(data) > 8:
                idx = 8
                bitslip = unpack("i", data[idx:idx+4])[0]
                print("Bitslip: " + str(bitslip))
            if len(data) > 12:
                idx = 12
                clock = unpack("i", data[idx:idx+4])[0]
                print("Clock: " + str(clock))
            data = b''
            for i in range(1, samples+1):
                #print(i)
                data += pack('i', i)
            print("Sending " + str(len(data)) + " bytes")
            conn.sendall(data)
            print("Data sent")
            print("Waiting...")
            time.sleep(1)
            data = b''
            for i in range(samples, 1-1, -1):
                #print(i)
                data += pack('i', i)
            print("Sending " + str(len(data)) + " bytes")
            conn.sendall(data)
            print("Data sent")
            print("Waiting...")
            time.sleep(1)
            data = b''
            for i in range(0, -1*samples, -1):
                #print(i)
                data += pack('i', i)
            print("Sending " + str(len(data)) + " bytes")
            conn.sendall(data)
            print("Data sent")
        conn.close()
except:
    s.close()
    raise
