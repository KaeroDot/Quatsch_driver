#!/usr/bin/python
#
# eth loop
# Copyright (C) 2018 INTI
# Copyright (C) 2018 Rodrigo A. Melo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import socket, argparse, re
from struct import *
#  import numpy as np
#  import matplotlib.pyplot as plt

###################################################################################################
# Parsing the command line
###################################################################################################

parser = argparse.ArgumentParser(
   prog        = 'read_adc',
   description = ''
)

parser.add_argument(
   '-a', '--address',
   metavar     = 'IP',
   default     = '192.168.0.10',
   help        = ''
)

parser.add_argument(
   '-p', '--port',
   metavar     = 'PORT',
   default     = '1000',
   type        = int,
   help        = ''
)

parser.add_argument(
   '-f', '--filename',
   metavar     = 'FILENAME',
   default     = 'samples',
   help        = ''
)

parser.add_argument(
   '-s', '--samples',
   metavar     = 'NUM_OF_SAMPLES',
   default     = '1024',
   help        = ''
)

parser.add_argument(
   '-t', '--test',
   action='store_true'
)

parser.add_argument(
   '-b', '--bitslip',
   action='store_true'
)

parser.add_argument(
   '-l', '--lut_index',
   action='store_true'
)

parser.add_argument(
   '-c', '--clock',
   metavar     = 'FMC16X Clock Source',
   default     = 0,
   type        = int,
   choices     = [1, 2, 3]
)

options = parser.parse_args()

###################################################################################################

TCP_IP   = options.address
TCP_PORT = options.port
filename = options.filename
test     = 1 if options.test else 0
bitslip  = 1 if options.bitslip else 0
lut_index= 1 if options.lut_index else 0
clock    = options.clock

try:
   samples = options.samples
   if re.search("k", samples, re.I):
      samples = re.sub('k', '', samples, 1,re.I)
      samples = int(samples)*1024
   elif re.search("m", samples, re.I):
      samples = re.sub('m', '', samples, 1,re.I)
      samples = int(samples)*1024*1024
   samples = int(samples)
   if samples%2 == 1:
      print("Samples should be even, rounding up")
      samples += 1
   if samples < 8:
      print("The min samples value must be at least 8, it was changed to 8")
      samples = 8
   if samples >= 2*1024*1024:
      print("The max samples value must be less than 2M, it was limited to 2M-4")
      samples = 2*1024*1024-4
except:
   print("There is an error with the NUM_OF_SAMPLES")
   print("It must be a integer, where you can use K (kilo) or M (mega)")
   exit()

print("Address: %s" % (TCP_IP))
print("Port:    %s" % (TCP_PORT))
print("Samples: %d" % (samples))
if test:
   print("Test data")
if bitslip:
   print("Bitslip")
if clock == 1:
   print("Internal clock with internal reference")
if clock == 2:
   print("Internal clock with external reference")
if clock == 3:
   print("External clock")

try:
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   s.connect((TCP_IP, TCP_PORT))
except:
   print("Connection refused")
   exit()

tx_buf  = pack("i",samples)
tx_buf += pack("i",test)
tx_buf += pack("i",bitslip)
tx_buf += pack("i",clock)

s.send(tx_buf)

fpa = open(filename+".txt", "w")
fpb = open(filename+".bin", "wb")

#  samplesarray = []
try:
   while samples > 0:
      if samples > 512:
         qty = 512
      else:
         qty = samples
      rx_buf = s.recv(qty*4)
      fpb.write(rx_buf)
      for i in range (0,qty):
          index = i*4
          rx_val  = str(unpack("h",rx_buf[index:index+2])[0])
          if lut_index:
             rx_val += " "
             rx_val += str(unpack("h",rx_buf[index+2:index+4])[0])
          fpa.write("%s\n" % (rx_val))
          #  samplesarray = samplesarray + [int(rx_val)]
      samples = samples - qty

except:
   print("Something was wrong, please try again")
   print("If the problem persist, change the NUM_OF_SAMPLES value")
   raise

#  plt.plot(samplesarray)
#  plt.show()

s.close()
fpa.close()
fpb.close()
